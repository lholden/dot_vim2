let $VIMHOME=expand('<sfile>:p:h')

runtime defaults.vim
runtime plugins.vim
runtime keybinds.vim

filetype plugin indent on
syntax enable

set directory=$VIMHOME/files/swap//
set viminfo='100,n$VIMHOME/files/viminfo

set encoding=utf8
set nobackup
set nowritebackup

set autoindent
set expandtab
set tabstop=2
set shiftwidth=2
set softtabstop=2

set backspace=indent,eol,start

set wrap
set linebreak
"set breakat=\ |@-+;,./?^I
set showbreak=~>

set ignorecase
set smartcase

set nohls
set title
set visualbell
set shortmess=atI
set laststatus=2
set modeline
set display=lastline
set noshowmode
set hidden
set signcolumn=yes

let g:netrw_banner=0
let g:netrw_winsize=15
let g:netrw_liststyle=3
let g:netrw_altv=1

""" Themes
let g:airline_theme='jellybeans'
let g:jellybeans_use_lowcolor_black=1
set background=dark
colorscheme jellybeans

hi SpellBad cterm=none ctermbg=88 gui=underline guibg=#401010 guisp=Red
hi SpellCap cterm=none ctermbg=20 gui=underline guibg=#000040 guisp=Blue
hi SpellRare cterm=none ctermbg=53 gui=underline guibg=#310041 guisp=Magenta
hi SpellLocal cterm=none ctermbg=23 gui=underline guibg=#003020 guisp=Cyan
hi Cursor guifg=black guibg=grey

"""
" Plugins
"""

""" Airline
let g:airline_powerline_fonts=1
let g:airline_left_sep = ''
let g:airline_right_sep = ''

""" Terminus
let g:TerminusBracketedPaste=0

""" Denite
if executable('rg')
  call denite#custom#var('file/rec', 'command', [
    \ 'rg', '--files', '--glob', '!.git'
    \ ])

  call denite#custom#var('grep', 'command', ['rg'])
  call denite#custom#var('grep', 'default_opts', ['--vimgrep', '--no-heading'])
  call denite#custom#var('grep', 'recursive_opts', [])
  call denite#custom#var('grep', 'pattern_opt', ['--regexp'])
  call denite#custom#var('grep', 'separator', ['--'])
  call denite#custom#var('grep', 'final_opts', [])
endif

call denite#custom#source('file/rec', 'matchers', [
  \ 'matcher/fuzzy',
  \ 'matcher/hide_hidden_files'
  \ ])

call denite#custom#source('file/rec', 'sorters', [
  \ 'sorter/sublime'
  \ ])

""" yoink
let g:yoinkIncludeDeleteOperations=1

""" NERDTree
let NERDTreeNaturalSort=1
let NERDTreeMinimalUI=1
let NERDTreeQuitOnOpen=1

autocmd bufenter * if (winnr("$") == 1 && exists("b:NERDTree") && b:NERDTree.isTabTree()) | q | endif

"""
" Languages
"""

""" vim-go
" temp disable as it doesn't seem to be getting the path correctly since
" updating
let g:go_fmt_autosave = 0

""" Rust
"let g:rustfmt_autosave = 1

""" vim-markdown
let g:markdown_fenced_languages = ['html', 'vim', 'ruby', 'python', 'bash=sh', 'rust']
let g:markdown_syntax_conceal = 0

""" vim-markdown-composer
let g:markdown_composer_autostart = 0

""" nginx
au BufRead,BufNewFile nginx.conf,*/nginx.d/*.conf if &ft == '' | setfiletype nginx | endif

""" jinja based template stuff
au BufNewFile,BufRead *.tera set ft=jinja
au BufNewFile,BufRead *.njk set ft=jinja
au BufNewFile,BufRead *.nj set ft=jinja

""" Jakefile .jake (javascript)
au BufNewFile,BufRead *.jake set ft=javascript

""" Jenkinsfile (groovy)
"au BufReadPost Jenkinsfile set syntax=groovy
"au BufReadPost Jenkinsfile set filetype=groovy

""" JSON
let g:vim_json_syntax_conceal = 0 

"""
" Other
"""

""" GUI and Term specific settings
if has("gui_running")
  set lines=40 columns=100 linespace=0

  if has("win32") || has("macunix")
    set guifont=InputMonoNarrow:h11
  else
    set guifont=Input\ Mono\ Narrow\ 11
  endif

  set guioptions-=T
  set guioptions-=m
  set guioptions-=e

  set mousemodel=popup
  set winaltkeys=no

  set spell
  set spelllang=en_us
else
  if &term =~ "xterm"
    set ttyfast
    if has("mouse_sgr")
      set ttymouse=sgr
    else
      set ttymouse=xterm2
    end

    exe "set t_te=" . &t_te . &t_op

    set t_Co=256

    let colorterm=$COLORTERM
    if colorterm =~ 'truecolor' || colorterm =~ '24bit'
      set termguicolors
      " also set escape characters for true colors, if needed
      if &term =~# '^screen'
        let &t_8f = "\<Esc>[38;2;%lu;%lu;%lum"
        let &t_8b = "\<Esc>[48;2;%lu;%lu;%lum"
      endif
    endif
  else
    colo default
    " Dont load terminus
    let g:TerminusLoaded=1
  end
end

