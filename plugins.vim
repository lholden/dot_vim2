let $DEIN_CACHE=resolve(expand('<sfile>:p:h:h').'/.cache/dein')
let $DEIN_PATH=resolve($DEIN_CACHE.'/repos/github.com/Shougo/dein.vim')

set runtimepath+=$DEIN_PATH

if dein#load_state($DEIN_CACHE)
  call dein#begin($DEIN_CACHE)
  call dein#add($DEIN_PATH)

  """ Core requirements
  if !has('nvim')
    call dein#add('roxma/nvim-yarp') " Yet Another Remote Plugin Framework
    call dein#add('roxma/vim-hug-neovim-rpc') " compatibility layer for neovim rpc client
  endif

  call dein#add('Shougo/vimproc.vim', {'build' : 'make'}) " Interactive command execution in Vim.

  """ Interface Plugins
  call dein#add('Shougo/denite.nvim') " Dark powered asynchronous unite all interfaces
  call dein#add('wincent/terminus') " Enhanced terminal integration
  call dein#add('vim-airline/vim-airline') " lean & mean status/tabline for vim that's light as air 
  call dein#add('vim-airline/vim-airline-themes') " A collection of themes for vim-airline 
  call dein#add('mhinz/vim-signify') " Show a diff using Vim its sign column. 
  call dein#add('drmikehenry/vim-fontsize') " Adjust Gvim font size via keypresses
  call dein#add('scrooloose/nerdtree') " A tree explorer plugin for vim. 
  call dein#add('Xuyuanp/nerdtree-git-plugin') " A plugin of NERDTree showing git status

  """ General Plugins
  call dein#add('tpope/vim-fugitive') " A Git wrapper so awesome, it should be illegal
  call dein#add('rbong/vim-flog') " A lightweight and powerful git branch viewer for vim. 
  call dein#add('tpope/vim-eunuch') " Helpers for UNIX
  call dein#add('tpope/vim-unimpaired') " Pairs of handy bracket mappings
  call dein#add('svermeulen/vim-cutlass') " adds a 'cut' operation separate from 'delete' 
  call dein#add('svermeulen/vim-yoink') " maintains a yank history to cycle between when pasting
  call dein#add('svermeulen/vim-subversive') " operator motions to quickly replace text 
  call dein#add('svermeulen/vim-repeat') " enable repeating supported plugin maps with '.'
  call dein#add('tmsvg/pear-tree') " A Vim auto-pair plugin that supports multi-character pairs, intelligent matching, and more 

  """ Themes
  call dein#add('nanotech/jellybeans.vim')

  """ Languages
  call dein#add('rust-lang/rust.vim')
  call dein#add('kchmck/vim-coffee-script')
  call dein#add('lepture/vim-jinja')
  call dein#add('martinda/Jenkinsfile-vim-syntax')
  call dein#add('pangloss/vim-javascript')
  call dein#add('fatih/vim-go')
  call dein#add('cespare/vim-toml')
  call dein#add('vim-jp/vim-cpp')
  call dein#add('octol/vim-cpp-enhanced-highlight')
  call dein#add('ekalinin/Dockerfile.vim')
  call dein#add('neovimhaskell/haskell-vim')
  call dein#add('othree/html5.vim')
  call dein#add('elzr/vim-json')
  call dein#add('chr4/nginx.vim')
  call dein#add('exu/pgsql.vim')
  call dein#add('stephpy/vim-yaml')
  call dein#add('leafgarland/typescript-vim')
  call dein#add('cakebaker/scss-syntax.vim')
  call dein#add('tpope/vim-markdown')
  call dein#add('hashivim/vim-terraform')
  call dein#add('chase/vim-ansible-yaml')
  call dein#add('elixir-editors/vim-elixir')
  call dein#add('justinj/vim-pico8-syntax')
  call dein#add('tpope/vim-rails')

  """ Finish plugins
  call dein#end()
  call dein#save_state()
endif

if !empty(dein#check_clean())
  call map(dein#check_clean(), "delete(v:val, 'rf')")
  call dein#recache_runtimepath()
endif

if dein#check_install()
  call dein#install()
endif
