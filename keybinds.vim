""" Cutlass
" Cutlass overrides the delete operations to actually just delete and not affect the current yank.

" Add an alias for deleting to the yank buffer.
nnoremap m d
xnoremap m d
nnoremap mm dd
nnoremap M D


""" Yoink
" Yoink will automatically maintain a history of yanks that you can choose between when pasting.

" Cycle through the paste history for the next paste
nmap <c-n> <plug>(YoinkPostPasteSwapBack)
nmap <c-p> <plug>(YoinkPostPasteSwapForward)

" Remap the p and P keys to use Yoink
nmap p <plug>(YoinkPaste_p)
nmap P <plug>(YoinkPaste_P)

" Toggle formatting
nmap <c-=> <plug>(YoinkPostPasteToggleFormat)

" Preserve cursor position when yanking
nmap y <plug>(YoinkYankPreserveCursorPosition)
xmap y <plug>(YoinkYankPreserveCursorPosition)


""" Subversive
" Subversive provides two new operator motions to make it very easy to perform quick substitutions.

" Lets you execute s<motion> to substitute the text object provided by the motion with the contents of the default
" register (or an explicit register if provided). For example, you could execute siw to replace the current word
" under the cursor with the current yank, or sip to replace the paragraph, etc.
nmap s <plug>(SubversiveSubstitute)
nmap ss <plug>(SubversiveSubstituteLine)
nmap S <plug>(SubversiveSubstituteToEndOfLine)

" Visual mode paste
xmap s <plug>(SubversiveSubstitute)
xmap p <plug>(SubversiveSubstitute)
xmap P <plug>(SubversiveSubstitute)


""" Unimpaired
" https://github.com/tpope/vim-unimpaired/blob/master/doc/unimpaired.txt


""" Denite
nnoremap <silent> <Leader>b :<C-u>Denite buffer<CR>
nnoremap <silent> <Leader>t :<C-u>Denite file/rec<CR>
nnoremap <silent> <Leader>g :<C-u>Denite grep<CR>

" Denite window insert mode key mappings
let s:denite_insert_mode_mappings = [
  \ ['jk',      '<denite:enter_mode:normal>',     'noremap'],
  \ ['<S-tab>', '<denite:move_to_previous_line>', 'noremap'],
  \ ['<Tab>',   '<denite:move_to_next_line>',     'noremap'],
  \ ['<C-k>',   '<denite:move_to_previous_line>', 'noremap'],
  \ ['<C-j>',   '<denite:move_to_next_line>',     'noremap'],
  \ ['<Up>',    '<denite:move_to_previous_line>', 'noremap'],
  \ ['<Down>',  '<denite:move_to_next_line>',     'noremap'],
  \ ]

for s:m in s:denite_insert_mode_mappings
  call denite#custom#map('insert', s:m[0], s:m[1], s:m[2])
endfor

""" NERDTree
" A tree explorer plugin for vim.

nnoremap <Leader>f :<C-u>NERDTreeToggle<CR>

""" Vanilla
" Adjust indentation
nnoremap <A-Right> >>
nnoremap <A-Left> <<
vnoremap <A-Right> >gv
vnoremap <A-Left> <gv
