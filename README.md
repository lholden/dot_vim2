```
sudo dnf install python3 python3-devel
pip3 install --user pynvim

cd ~/
curl https://raw.githubusercontent.com/Shougo/dein.vim/master/bin/installer.sh > installer.sh
sh ./installer.sh ~/.cache/dein

git clone git@gitlab.com:lholden/dot_vim2.git .vim
```
